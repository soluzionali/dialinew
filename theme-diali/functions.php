<?php
/**
 * Diali Theme functions and definitions
 *
 * @package Diali
 * @since 1.0.0
 */

add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
    wp_enqueue_style( 'theme-css', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'diali-css', get_stylesheet_directory_uri() . '/assets/css/styles.css');
}

function theme_enqueue_script() {
	wp_enqueue_script( 'diali-js', get_stylesheet_directory_uri() . '/assets/js/main.js', '',true );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_script' );

add_action( 'wp_enqueue_scripts', function() {
	$styles = wp_styles();
	$styles->add_data( 'twentytwenty-style', 'after', array() );
}, 20 );


class MyTheme_TopBar_Customize{
  public static function register ( $wp_customize ) {
      $wp_customize->add_section('sf_top_bar', array(
              'title'      => __( 'Info Bar', 'storefront-child' ),
              'priority'   => 30,
          ));
      $wp_customize->add_setting('sf_child_phone', array(
          'default'        => '',
          'capability'     => 'edit_theme_options',
          'type'           => 'option',
      ));
      $wp_customize->add_setting('sf_child_contact', array(
              'default'        => 'info@domain.com',
              'capability'     => 'edit_theme_options',
              'type'           => 'option',
          ));
      $wp_customize->add_setting('sf_child_twitter', array(
            'default'        => 'www.twitter.com',
            'capability'     => 'edit_theme_options',
            'type'           => 'option',
        ));
      $wp_customize->add_setting('sf_child_instagram', array(
          'default'        => 'www.instagram.com',
          'capability'     => 'edit_theme_options',
          'type'           => 'option',
      ));
      $wp_customize->add_setting('sf_child_linkedin', array(
        'default'        => 'www.linkedin.com',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',
      ));
      $wp_customize->add_control(
          new WP_Customize_Color_Control($wp_customize,'sf_child_phone',
              array(
                  'label'      => __('Phone', 'storefront-child'),
                  'section'    => 'sf_top_bar',
                  'settings'   => 'sf_child_phone',
                  'priority'   => 10,
                  'type'       => 'text'
              )) ); 
      $wp_customize->add_control(
          new WP_Customize_Color_Control($wp_customize,'sf_child_contact',
              array(
                  'label'      => __('Email', 'storefront-child'),
                  'section'    => 'sf_top_bar',
                  'settings'   => 'sf_child_contact',
                  'priority'   => 10,
                  'type'       => 'text'
              )) );
      $wp_customize->add_control(
        new WP_Customize_Color_Control($wp_customize,'sf_child_twitter',
            array(
                'label'      => __('Twitter', 'storefront-child'),
                'section'    => 'sf_top_bar',
                'settings'   => 'sf_child_twitter',
                'priority'   => 10,
                'type'       => 'text'
            )) );
      $wp_customize->add_control(
        new WP_Customize_Color_Control($wp_customize,'sf_child_instagram',
            array(
                'label'      => __('Instagram', 'storefront-child'),
                'section'    => 'sf_top_bar',
                'settings'   => 'sf_child_instagram',
                'priority'   => 10,
                'type'       => 'text'
            )) );
      $wp_customize->add_control(
        new WP_Customize_Color_Control($wp_customize,'sf_child_linkedin',
            array(
                'label'      => __('Linkedin', 'storefront-child'),
                'section'    => 'sf_top_bar',
                'settings'   => 'sf_child_linkedin',
                'priority'   => 10,
                'type'       => 'text'
            )) );
  }
  
  public static function top_bar_html( $wp_customize ){       
      $phone=get_option('sf_child_phone');
      $mail=get_option('sf_child_contact');
      $twitter=get_option('sf_child_twitter');
      $instagram=get_option('sf_child_instagram');
      $linkedin=get_option('sf_child_linkedin');?>
      <div class="sf_top_bar">
         <a class='top_bar-mail' href='mailto:<?php echo $mail;?>'><?php echo $mail;?></a>
         <a class='top_bar-phone' href='tel:<?php echo $phone;?>'><?php echo $phone;?></a>
         <div class='top_networks'>
            <a class='top_networks-twitter' target='_blank' href='<?php echo $twitter;?>'>T</a>
            <a class='top_networks-instagram' target='_blank' href='<?php echo $instagram;?>'>I</a>
            <a class='top_networks-linkedin' target='_blank' href='<?php echo $linkedin;?>'>L</a>
         </div>
      </div>    
<?php }
}
add_action( 'customize_register' , array( 'MyTheme_TopBar_Customize' , 'register' ) );
// add_action('wp_head',array( 'MyTheme_TopBar_Customize' , 'inject_css' ));
add_action('storefront_before_header', array( 'MyTheme_TopBar_Customize' , 'top_bar_html' ));
add_action( 'wp_head', array( 'MyTheme_TopBar_Customize' , 'top_bar_html' ) );


add_filter('wpcf7_autop_or_not', '__return_false');

  // add_filter( 'body_class', 'extra_body_class' );
  // // Add specific CSS class by filter
  // function extra_body_class( $classes ) {
  //   if(is_page('6') || is_page('316')){
  //     $classes[] = 'taskgo-help';
  //   }
  //   return $classes;
  // }

?>