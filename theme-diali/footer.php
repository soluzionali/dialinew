<?php
/**
 * The template for displaying the footer
 *
 * Contains the opening of the #site-footer div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty
 * @since Twenty Twenty 1.0
 */
	$twitter=get_option('sf_child_twitter');
	$instagram=get_option('sf_child_instagram');
	$linkedin=get_option('sf_child_linkedin');

?>
			<footer id="site-footer" role="contentinfo" class="header-footer-group">

				<div class="section-inner">

					<div class='footer-logo'>
						<?php
							// Site title or logo.
							twentytwenty_site_logo();
						?>
					</div>

					<div class='top_networks'>
						<a class='top_networks-twitter' target='_blank' href='<?php echo $twitter;?>'>T</a>
						<a class='top_networks-instagram' target='_blank' href='<?php echo $instagram;?>'>I</a>
						<a class='top_networks-linkedin' target='_blank' href='<?php echo $linkedin;?>'>L</a>
					</div>

					<div class="footer-credits">

						<p class="footer-copyright">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a> &copy;
							<span>Todos los derechos reservados</span>
							<?php
							echo date_i18n(
								/* translators: Copyright date format, see https://www.php.net/manual/datetime.format.php */
								_x( 'Y', 'copyright date format', 'twentytwenty' )
							);
							?>
						</p><!-- .footer-copyright -->


					</div><!-- .footer-credits -->



				</div><!-- .section-inner -->

			</footer><!-- #site-footer -->

		<?php wp_footer(); ?>

	</body>
</html>
