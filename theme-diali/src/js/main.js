window.addEventListener('load', function() {

    var itm = document.querySelector(".sf_top_bar");
    var cln = itm.cloneNode(true);
    
    document.querySelector(".menu-modal .menu-bottom").appendChild(cln);

    var newsStr = window.location.pathname;

    if(newsStr.includes('novedades')){
        document.querySelector('.header-inner .primary-menu>li.nav-news').classList.add('current-menu-item');
    }
    if(newsStr.includes('servicio')){
        document.querySelector('.header-inner .primary-menu>li.nav-services').classList.add('current-menu-item');
    }

});