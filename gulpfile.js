var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    cache = require('gulp-cache'),
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),
    sourcemaps = require("gulp-sourcemaps"),
    concat = require('gulp-concat');

var css = { 
    sassOpts: {
        outputStyle: 'expanded',
        precision: 3    
    }
};

gulp.task('scripts', function(){
  return gulp.src(['theme-diali/src/js/**/*.js'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        gulp.emit('end');
    }}))
    //.pipe(gulp.dest(pathDist+'js/'))
    //.pipe(rename({suffix: '.min'}))
    .pipe(uglify())
    .pipe(gulp.dest('theme-diali/assets/js/'))
});

gulp.task('styles', function(){
  return gulp.src(['theme-diali/src/scss/*.scss'])
    .pipe(plumber({
      errorHandler: function (error) {
        console.log(error.message);
        gulp.emit('end');
    }}))
    .pipe(sass(css.sassOpts))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('theme-diali/assets/css/'))
});

gulp.task('watch', function(){
  gulp.watch('theme-diali/src/scss/**/*.scss', gulp.series('styles'));
  gulp.watch('theme-diali/src/js/**/*.js', gulp.series('scripts'));
})

gulp.task(
  'default',
  gulp.series('watch')
  // gulp.series('sass_styles','js_libs', 'js_vendor', 'scripts_mod')
);

// gulp.task('default', ['scripts','OldScripts','OldStyles','browser-sync'], function(){
//   gulp.watch(pathDist+"css/**/*.scss", ['styles']);
//   gulp.watch(pathDist+"js/**/*.js", ['scripts']);
//   gulp.watch(oldpathDist+"js/*.js", ['OldScripts']);
//   gulp.watch(pathSource+"*.html", ['bs-reload']);
// });